<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->firstName, // generate first name
            'last_name' => $this->faker->lastName, // generate last name
            'email' => $this->faker->unique()->safeEmail, // generate safe email address ( due to unique attribute on email column )
        ];
    }
}
