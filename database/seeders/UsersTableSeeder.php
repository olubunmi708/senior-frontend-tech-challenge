<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // run checker
        $this->checkUsersRecord();
    }


    /**
     * Check users records if it exists or not
     * To notify runner of users count limit
     * And to avoid unwanted records
     */
    private function checkUsersRecord()
    {
        // Get total users count in database ( returns 0 if no record found )
        $usersCount = \App\Models\User::count();

        // If users records are found, notify via terminal to avoid creating more than 10 users.
        if ($usersCount > 0) {
            // call notifier method
            $this->notifyRunner();
            die();
        }

        // No user records are found, call createUser method to create users record.
        $this->createUsers();
    }


    /**
     * notify and print record status messages
     */
    private function notifyRunner()
    {
        // print message
        $this->command->warn('You already have users records in database.');

        // Ask if to wipe table and reseed new ones
        if ( $this->command->confirm(
            'Will you like to remove existing user records and seed new ones?',
            'Yes')
        ) {
            // call method to wipe existing records
            $this->wipeUsersTable();
            // print message
            $this->command->info('Users records have been wiped.');
        }

        // call createUser method to create users record.
        $this->createUsers();
    }

    /**
     * Wipe users record
     */
    private function wipeUsersTable()
    {
        // Wipe records
        \App\Models\User::query()->truncate();
    }

    /**
     * Create fresh users record
     */
    private function createUsers()
    {
        // Call laravel user factory to create demo users
        \App\Models\User::factory(10)->create();
        // Print message
        $this->command->info('Users data created successfully!');
    }
}
