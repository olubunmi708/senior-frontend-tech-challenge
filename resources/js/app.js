/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import { createApp } from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';

import Users from './components/Users.vue';
import Header from './components/Header.vue';

require('./bootstrap');

createApp(Users).use(VueAxios, axios).mount('users');
createApp(Header).mount('app-header');
